package corso.omicron.HibernateOneToManyBase.models.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import corso.omicron.HibernateOneToManyBase.models.Carta;
import corso.omicron.HibernateOneToManyBase.models.Persona;

public class GestoreSessioni {

	private static GestoreSessioni ogg_gestore;
	private SessionFactory factory;
	
	public static GestoreSessioni getIstanza() {
		if(ogg_gestore == null) {
			ogg_gestore = new GestoreSessioni();
		}
		
		return ogg_gestore;
	}
	
	public SessionFactory getFactory() {
		if(factory == null) {
			
			factory = new Configuration()
					.configure("/resources/hibernate.cfg.xml")
					.addAnnotatedClass(Persona.class)
					.addAnnotatedClass(Carta.class)
					.buildSessionFactory();
			
		}
		
		return factory;
	}
	
}
