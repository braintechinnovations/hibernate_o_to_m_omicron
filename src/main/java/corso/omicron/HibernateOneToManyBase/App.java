package corso.omicron.HibernateOneToManyBase;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.omicron.HibernateOneToManyBase.models.Carta;
import corso.omicron.HibernateOneToManyBase.models.Persona;
import corso.omicron.HibernateOneToManyBase.models.db.GestoreSessioni;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

    	SessionFactory factory = GestoreSessioni.getIstanza().getFactory();
    	Session sessione = factory.getCurrentSession();
    	
    	//Inserimento
//    	Persona mario = new Persona("Mario", "Rossi", "MRRRSS");
//    	
//    	try {
//    	
//    		sessione.beginTransaction();
//    		
//    		sessione.save(mario);
//    		
//    		sessione.getTransaction().commit();
//    		
//    	} catch (Exception e) {
//    		System.out.println(e.getMessage());
//		}
    	
    	//Ricerca
//    	try {
//    		
//    		sessione.beginTransaction();
//    		
//    		Persona temp = sessione.get(Persona.class, 1);
//    		System.out.println(temp);
//    		
//    		sessione.getTransaction().commit();
//    		
//    	} catch (Exception e) {
//    		System.out.println(e.getMessage());
//		}
    	
    	//Inserimento Carta
//    	Carta carUno = new Carta("Tudodì", "TD123456");
//    	
//    	try {
//    		sessione.beginTransaction();
//    		
//    		sessione.save(carUno);
//    		
//    		sessione.getTransaction().commit();
//    		
//    	} catch (Exception e) {
//    		System.out.println(e.getMessage());
//		}
    	
    	
    	//Inserimento di persona e carta!
//    	Persona valeria = new Persona("Valeria", "Verdi", "VLRVRD");
//    	Carta carTgt = new Carta("Tigotà", "TG123456", valeria);
//    	
//    	try {
//			sessione.beginTransaction();
//			
//			sessione.save(valeria);
//			sessione.save(carTgt);
//			
//			sessione.getTransaction().commit();
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//		}
    	
    	//Recupero della carta e del proprietario
    	try {
			sessione.beginTransaction();
			
			Carta cartaTemp = sessione.get(Carta.class, 10);
			System.out.println(cartaTemp);
			
//			Persona persTemp = sessione.get(Persona.class, 3);
//			System.out.println(persTemp);
			
			sessione.getTransaction().commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
    	
    	sessione.close();
    	
    }
}
