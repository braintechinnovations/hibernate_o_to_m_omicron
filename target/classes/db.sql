-- Realizzione della relazione One To Many
DROP DATABASE IF EXISTS carte_fedelta;
CREATE DATABASE carte_fedelta;
USE carte_fedelta;

CREATE TABLE persona(
	personaID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(250) NOT NULL,
    cognome VARCHAR(250) NOT NULL,
    cod_fis VARCHAR(250) UNIQUE
);

CREATE TABLE carta(
	 cartaID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
     numero VARCHAR(25) NOT NULL,
     negozio VARCHAR(250) NOT NULL,
     persona_rif INTEGER,
     FOREIGN KEY (persona_rif) REFERENCES persona(personaID)
);

INSERT INTO persona (nome, cognome, cod_fis) VALUES ("Giovanni", "Pace", "ABCDEF");

INSERT INTO carta(numero, negozio) VALUES
("CD123456",	"Conad"),
("CD123458",	"Conad"),
("CD123459",	"Conad"),
("AB123456",	"Coop"),
("AB123458",	"Coop"),
("AB123459",	"Coop");

INSERT INTO carta(numero, negozio, persona_rif) VALUES
("CD123457",	"Conad", 1),
("AB123457",	"Coop", 1);

SELECT * FROM persona;

SELECT * FROM carta
	JOIN persona ON carta.persona_rif = persona.personaID;